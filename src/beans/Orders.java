package beans;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@ApplicationScoped
public class Orders {
	
	public List<Order> orders;

	public Orders() {
		super();
		orders = new ArrayList<Order>();
		orders.add(new Order("123", "Key", 5.47f, 10));
	}
	public Order getOrder() {
		return this.orders.get(0);
	}
	
}
