package controllers;

import javax.ejb.EJB;
import java.sql.*;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.CustomScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;
import javax.inject.Inject;

import beans.Order;
import beans.User;
import business.AnotherOrdersBusinessService;
import business.MyTimerService;
import business.OrdersBusinessInterface;
import business.OrdersBusinessService;

@ManagedBean
@ViewScoped
public class FormController {
	@Inject
	OrdersBusinessInterface ordInt;
//	@EJB
//	MyTimerService timer;
	public OrdersBusinessInterface getService() {
		return ordInt;
	}
	private void getAllOrders( ) {
		try {
			//establish database connection
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "Magrols1!");
			//print success
			System.out.println("Success.");
			
			
			String query = "SELECT * FROM testapp.ORDERS";
			
			Statement stmt = con.createStatement();
			
			ResultSet rs=stmt.executeQuery(query);  
			
			while(rs.next())
			{
				System.out.println(rs.getInt(1)+"  "+rs.getString(3)+"  "+rs.getFloat(4));
			}
			con.close();  
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Failure.");
		}
				
	}
	private void insertOrder() {
		try {
			Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "Magrols1!");

			System.out.println("Success.");
			
			String query = "INSERT INTO testapp.ORDERS(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES('001122334455', 'This was inserted new', 25.00, 100)";
			
			Statement stmt = con.createStatement();
			
			stmt.executeUpdate(query);  
			
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Failure.");
		}
				
	}
	
	public String onSendOrder() 
	{
		ordInt.sendOrder(new Order("1234", "Bowflex", 10.0f, 5));
		return "OrderResponse.xhtml";
	}
	public String onLogoff() {
		// Invalidate the Session to clear the security token
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			
		// Redirect to a protected page (so we get a full HTTP Request) to get Login Page
		return "TestResponse.xhtml?faces-redirect=true";

	}
}
