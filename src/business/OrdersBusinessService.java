package business;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import beans.Order;
import beans.Orders;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@LocalBean
@Alternative
public class OrdersBusinessService implements OrdersBusinessInterface {
	
	@Resource(mappedName="java:/ConnectionFactory")
	private ConnectionFactory connectionFactory;
	
	@Resource(mappedName="java:/jms/queue/Order")
	private Queue queue;


    List<Order> orders;
    
    public String verse = "The Lord is my shepherd; I shall not want. He maketh me to lie down in green pastures: he leadeth me beside the still waters. He restoreth"
    		+ "my soul: he leadeth me in the paths of righteousness for his names sake. yeah, though I walk through the valley of the shadow of death, I will fear no evil: "
    		+ "for thou art with me; thy rod and thy staff they comfort me. "
    		+ "thou preparest a table before me in the presence of mine enemies: thou anointest my head with oil; my cup runneth over. "
    		+ "Surely goodness and mercy shall follow me all the days of my life: and I will dwell in the house of the Lord for ever.";
    public OrdersBusinessService() {
		super();
		orders = new ArrayList<Order>();
		orders.add(new Order("123", "Key", 5.47f, 10));
    }

	/**
     * @see OrdersBusinessInterface#test()
     */
	@Override
	public void test() {
		System.out.println("Hello from OrdersBusinessService");
	}

	@Override
	public List<Order> getOrders() {
		return orders;
	}

	@Override
	public void setOrders(List<Order> orders) {
		this.orders = orders;
		
	}

	@Override
	public void sendOrder(Order order) {
		
		// Send a Message for an Order
		try 
		{
			Connection connection = connectionFactory.createConnection();
			Session  session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			MessageProducer messageProducer = session.createProducer(queue);
			TextMessage message1 = session.createTextMessage();
			message1.setText("This is test message");
			messageProducer.send(message1);
			connection.close();
		} 
		catch (JMSException e) 
		{
			e.printStackTrace();
		}

		
	}
	public boolean getWordCount(String word) 
	{
		if (this.verse.contains(word)) 
		{
			return true;
		}
		else {return false;}
	}
}
